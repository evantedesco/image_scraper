class Url < ActiveRecord::Base
  belongs_to :job
  has_many :images
  scope :completed, -> { where(status: 'completed') }
  scope :inprogress, -> { where(status: 'inprogress') }

  validates :address, format: {with: URI::ABS_URI, message: "Address must be a fully qualified URL"}

  def update_uri(uri)
    return unless address != uri
    update_attributes(address: uri)
  end
end