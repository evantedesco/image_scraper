class Scraper
  require 'mechanize'

  def self.scrape(uris, crawl_depth)
    @output ||= []
    return @output.uniq unless crawl_depth > 0
    links = find_links(uris)
    @output.concat(links)
    crawl_depth -= 1
    scrape(links, crawl_depth)
  end

  def self.scrape_images(uri)
    page = get_page(uri)
    return [] unless page.class == Mechanize::Page
    base_url = page.uri.to_s
    images = page.search('//img') || []
    qualify_images(uri, images).push(base_url)
  end

  private
  def self.find_links(uris)
    uris.map do |uri|
      scrape_links(uri)
    end.compact.flatten
  end

  def self.scrape_links(uri)
    page = get_page(uri)
    return [] unless page.class == Mechanize::Page
    base_url = page.uri.to_s
    links = page.links.map(&:href).push(base_url) || []
    links.map { |link| qualify_link(base_url, link) }.compact.flatten
  end

  def self.qualify_link(base_url, link)
    return unless link
    link = link[0] == "/" ? "#{base_url[0][0..-1]}#{link}" : link
    link if http?(link)
  end


  def self.qualify_images(base_uri, images)
    images.map do |image|
      next unless has_src?(image)
      qualify_image(base_uri, image)
    end.compact
  end

  def self.qualify_image(base_uri, image)
    src = image.attributes["src"].value
    if src =~ /^\/[\/]/
      result = "#{scheme(base_uri)}#{src}"
    elsif src =~ /^\//
      result = "#{base_uri}#{src}"
    else
      result = src
    end
    http?(result) ? result : nil
  end

  def self.has_src?(image)
    image.attributes["src"].value
  rescue NoMethodError
    false
  end

  def self.scheme(uri)
    uri = URI.parse(uri)
    "#{uri.scheme}:"
  end

  def self.http?(uri)
    uri = URI.parse(uri)
    uri.kind_of?(URI::HTTP)
  rescue URI::InvalidURIError
    false
  end

  def self.get_page(uri)
    agent = Mechanize.new
    agent.gzip_enabled = false
    agent.user_agent_alias = 'Mac Safari'
    agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    agent.get(uri)
  rescue Mechanize::ResponseCodeError
    puts '#{uri} is not a valid URI'
  rescue  => e
    puts e.message
  end
end