class Image < ActiveRecord::Base
  belongs_to :url
  validates :address, format: { with: URI::ABS_URI , message: "Address must be fully qualified" }
  validates :address, format: {with:  %r{\.(png|jpg|jpeg)\z}i, :message => "image must be a png, jpg, or jpeg"}
end