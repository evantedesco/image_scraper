class Job < ActiveRecord::Base
  has_many :urls
  has_many :images, through: :urls

  def get_status
    status = Hash.new
    status[:completed] = urls.completed.count
    status[:inprogress] = urls.inprogress.count
    status
  end

  def to_param
    uuid
  end
end