json.id @job.uuid
if @job.status == "completed"
  json.set! :results do
    @job.urls.each do |url|
      json.set! url.address do
        json.array!(url.images.map(&:address))
      end
    end
  end
else
  json.message "Your job is still processing.  See the status at /jobs/:id/status"
end
