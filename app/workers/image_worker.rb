class ImageWorker
  include Sidekiq::Worker

  def perform(url_id, uri, last_worker)
    url = Url.find(url_id)
    image_urls = Scraper.scrape_images(uri)
    if image_urls.present?
      final_uri = image_urls.last
      url.update_uri(final_uri)
      image_urls.map { |image| Image.create(url_id: url.id, address: image) }
    end
    url.update_attributes(status: "completed")
    url.job.update_attributes(status: "completed") if last_worker
  end
end