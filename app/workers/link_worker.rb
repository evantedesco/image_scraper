class LinkWorker
  include Sidekiq::Worker

  def perform(job_id, crawl_depth, urls )
    links = Scraper.scrape(urls, crawl_depth)
    urls = links.map { |link| Url.create(address: link, job_id: job_id, status: "inprogress") }
    last_url = urls.pop
    urls.map{|url| ImageWorker.perform_async(url.id, url.address, false) }
    ImageWorker.perform_async(last_url.id, last_url.address, true)
  end
end