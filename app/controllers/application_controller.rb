class ApplicationController < ActionController::Base
  protect_from_forgery
  skip_before_action :verify_authenticity_token, if: :json_request?
  rescue_from JSON::ParserError, with: :json_parse_error

  def json_request?
    request.format.json?
  end

  private
  def json_parse_error(error)
    render :json => {:error => "Invalid Json: #{error.message}"}, :status => :bad_request
  end
end
