class JobsController < ApplicationController

  def create
    params = JSON.parse(request.body.read)
    @job = Job.new(uuid: SecureRandom.uuid, crawl_depth: 1)

    if @job.save
      LinkWorker.perform_async(@job.id, @job.crawl_depth, params['urls'])

      render json: {id: @job.uuid}.as_json, status: 202
    else
      render nothing: true, status: 404
    end
  end

  def status
    @job = Job.find_by_uuid(params[:id])

  end

  def results
    @job = Job.where(uuid: params[:id]).includes(:urls).includes(:images).first
  end

  def job_params
    params.require(:urls)
  end
end
