class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :crawl_depth, default: 1
      t.string :status
      t.uuid :uuid
      t.timestamps
    end
  end
end
