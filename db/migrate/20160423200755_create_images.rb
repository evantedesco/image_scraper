class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :address
      t.references :url, null: false
      t.timestamps
    end
  end
end
