https://image-scraper-app.herokuapp.com
https://evantedescollc.statuspage.io/


==Explaining Design Decisions For Fun And Profit

*For the sake of brevity I am going to outline some of my design decisions as a list.

1) I think the most obvious design decision I made was to break the task up into 2 separate jobs.  By using a separate job for scraping links and images, I was able to improve the potential for concurrency in the future.

2) I Decided to add a rescue for Json parse errors that shows a helpful error message.  I have been on the wrong side of a 400 too many times hoping for a hint from the “documentation” to not handle malformed Json errors.

3) I did my best to follow Sandi Metz’s 5 rules but there were some places where I had to enact rule 0.

4) I am honestly not sure how I feel about my method of pushing and popping the url from the images worker.  I initially wrote it to return the urls separately from the base_url but it seems that since they were often being passed around together that they were one object.  I also considered using a hash but it didn’t feel right with the relationship for base_urls and a link on the page is a sibling relationship.

5) I used Sidekiq because it is the most performant background process available.

6) I used mechanize because it was easy to use and screen scraping was not something I wanted to have to deal with given the constraints.

7) I Integrated with New Relic so I could get a status page on my application for obvious reasons.


==Possible improvements also as a list for all of our sake.

1) I initially wanted to dockerize this application, but I got hung up on what I though was a bug but turned out to simply be me not being patient and allowing the processes to finish.  In attempting to diagnose this “bug” I did find out that:

2)  Mechanize is not thread safe apparently.  If there were to be a production ready application a more suitable client would likely be needed.

3)  A more performant language that handles concurrency and multithreading better than Ruby could really improve response times if this thing needed to scale.  GO comes to mind.

4) In retrospect I could have broken the Scraper class into LinkScraper, ImageScraper, and UrlValidator classes.

5) I should really improve the test suite.  The majority of my specs were from happy path testing.  Usually I would take the time to test the edge cases in order to document the code and identify more potential failure points but I was unable to given the restraints.

6) I could improve my file extension validation to account for query parameters.

7) I am sure there is more that code climate would pick up as far as style and possibly quotations.