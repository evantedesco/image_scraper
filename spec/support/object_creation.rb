class ObjectCreation
  def self.create_url(args={})
    defaults = {
      address: "http://www.reddit.com#{rand(1000)}",
      status: "inprogress",
      job_id: rand(1000)
    }
    Url.create!(defaults.merge!(args))
  end

  def self.create_job(args={})
    defaults = {
      uuid: SecureRandom.uuid,
      status: "inprocess"
    }

    Job.create!(defaults.merge!(args))
  end

  def self.create_image(args={})
    defaults = {
      address: "http://www.reddit.com#{rand(1000)}.jpg",
      url_id: rand(1000)
    }

    Image.create!(defaults.merge!(args))
  end
end