require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe JobsController, type: :controller do
  describe 'POST #index' do
    let(:json_body) { '{"urls":["http://www.massmetrics.com/about", "https://google.com","https://www.statuspage.io"]}' }
    let(:headers) { {format: :json} }

    it 'returns a 202' do
      Sidekiq::Testing.fake!

      post :create, json_body, headers

      expect(response).to have_http_status(202)
    end

    it 'creates a job' do
      Sidekiq::Testing.fake!
      expect { post :create, json_body, headers }.to change { Job.count }.by(1)
    end

    it 'Saves the urls from the params' do
      Sidekiq::Testing.inline! do
        allow(Scraper).to receive(:scrape_images).and_return([])
        allow(Scraper).to receive(:scrape).and_return(['https://www.chase.com/', 'http://www.massmetrics.com/about'])
        post :create, json_body, headers

        job = Job.find_by_uuid(JSON.parse(response.body)['id'])
        urls = job.urls.map(&:address)

        expect(urls.count).to eq(2)
      end
    end

    it 'saves the URI of the page that is actually crawled' do
      Sidekiq::Testing.inline! do
        allow(Scraper).to receive(:scrape_images).and_return(['https://www.chase.com/'])
        post :create, '{"urls":["http://www.chase.com"]}', headers

        expect(Url.find_by_address('http://www.chase.com')).to be_nil
        expect(Url.find_by_address('https://www.chase.com/')).to be_present
      end
    end

    it 'returns a valid UUID' do
      Sidekiq::Testing.fake!
      post :create, json_body, headers

      uuid_regex = Regexp.new(/^[0-9a-f]{8}-?[0-9a-f]{4}-?[1-5][0-9a-f]{3}-?[89ab][0-9a-f]{3}-?[0-9a-f]{12}$/i)
      uuid = JSON.parse(response.body)['id']

      expect(uuid_regex.match(uuid).to_s).to eq(uuid)
    end

    it 'Gives a helpful error for invalid json' do
      Sidekiq::Testing.fake!
      invalid_json = {urls: ["https://google.com", "https://www.statuspage.io"]}
      post :create, invalid_json, headers
      result = JSON.parse(response.body)
      error = result["error"]

      expect(error).to eq("Invalid Json: 757: unexpected token at 'urls%5B%5D=https%3A%2F%2Fgoogle.com&urls%5B%5D=https%3A%2F%2Fwww.statuspage.io'")
    end

    it 'returns a 400 for invalid json' do
      Sidekiq::Testing.fake!
      invalid_json = {urls: ["https://google.com", "https://www.statuspage.io"]}
      post :create, invalid_json, headers

      expect(response).to have_http_status(400)
    end

    it 'returns a 404 if the request fails' do
      allow_any_instance_of(Job).to receive(:save).and_return(false)
      post :create, json_body, headers

      expect(response).to have_http_status(404)
    end

    it 'Does not raise if the URL is not fully qualified' do
      Sidekiq::Testing.fake!

      expect { post :create, '{"urls":["chase.com", "https://google.com"]}', headers }.to_not raise_error
    end
  end

  describe 'GET#status' do
    let(:job) { ObjectCreation.create_job }
    it 'returns a response' do
      get :status, id: job.uuid, format: :json

      expect(response).to have_http_status(200)
    end

    it 'returns the UUID of the job' do
      get 'status', {id: job.uuid, format: :json}

      id = JSON.parse(response.body)["id"]
      expect(id).to eq(job.uuid)
    end

    it 'returns the status of urls that belong to the job' do
      ObjectCreation.create_url(job_id: job.id, status: 'completed')
      ObjectCreation.create_url(job_id: job.id, status: 'inprogress')
      get 'status', {id: job.uuid, format: :json}
      result = JSON.parse(response.body)

      expect(result).to eq({"id" => "#{job.uuid}", "status" => {"completed" => 1, "inprogress" => 1}})
    end
  end

  describe 'GET#results' do
    let(:job) { ObjectCreation.create_job(status: 'completed') }
    it 'returns a response' do
      ObjectCreation.create_url(job_id: job.id, status: 'completed')
      ObjectCreation.create_url(job_id: job.id, status: 'completed')
      get 'results', {id: job.uuid, format: :json}

      expect(response).to have_http_status(200)
    end

    it 'returns the uuid of the job' do
      get 'results', {id: job.uuid, format: :json}
      id = JSON.parse(response.body)['id']

      expect(id).to eq(job.uuid)
    end

    it 'returns the full results' do
      url_1 = ObjectCreation.create_url(job_id: job.id, status: 'completed')
      url_2 = ObjectCreation.create_url(job_id: job.id, status: 'completed')
      image_1 = ObjectCreation.create_image(url_id: url_1.id)
      image_2 = ObjectCreation.create_image(url_id: url_1.id)
      image_3 = ObjectCreation.create_image(url_id: url_2.id)
      get 'results', {id: job.uuid, format: :json}
      result = JSON.parse(response.body)
      url_1_images = result["results"]["#{url_1.address}"]
      url_2_images = result["results"]["#{url_2.address}"]

      expect(url_1_images).to include(image_1.address, image_2.address)
      expect(url_2_images).to include(image_3.address)
    end

    it 'lets the user know if the job is still processing' do
      job = ObjectCreation.create_job(status: "inprocess")
      get 'results', {id: job.uuid, format: :json}
      result = JSON.parse(response.body)

      expect(result["message"]).to eq("Your job is still processing.  See the status at /jobs/:id/status")
    end
  end
end
