require 'rails_helper'

RSpec.describe Image do
  it 'is invalid without a fully qualified address' do
    invalid_link = Image.new(address: "reddit.com", url_id: 1)

    expect(invalid_link).to_not be_valid
  end

  it 'is valid with a fully qualified address' do
    valid_link = Image.new(address: "https://www.reddit.com.jpg", url_id: 1)

    expect(valid_link).to be_valid
  end

  it 'is invalid without the correct file extension' do
    invalid_link = Image.new(address: "reddit.com.svg", url_id: 1)

    expect(invalid_link).to_not be_valid
  end
end