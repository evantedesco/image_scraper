require 'rails_helper'

RSpec.describe Scraper do

  it 'returns a list of links when passed a URL' do
    expect(Scraper.scrape_links('http://www.example.com/')).to eq(["http://www.iana.org/domains/example", "http://www.example.com/"])
  end

  it 'handles redirects' do
    expect(Scraper.scrape_links('http://www.chase.com/')).to include('https://www.chase.com/')
  end

  it 'returns a list of images when passed a URL' do
    expect(Scraper.scrape_images('http://www.massmetrics.com/about')).to include('http://www.massmetrics.com/about/assets/product-sort-0c6499f0b96adc1d496a1250bb2c2c75.png')
  end

  it 'qualifies relative paths the image urls' do
    image_url = Scraper.scrape_images('http://www.massmetrics.com/about').first

    expect(URI::ABS_URI.match(image_url).to_s).to eq(image_url)
  end

  it 'qualifies paths for cdn supplied images' do
    image_url = Scraper.scrape_images('http://www.statuspage.io').first

    expect(URI::ABS_URI.match(image_url).to_s).to eq(image_url)
  end
end