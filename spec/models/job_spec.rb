require 'rails_helper'

RSpec.describe Job do
  let(:job) { ObjectCreation.create_job }
  it 'returns the status of completed jobs' do
    ObjectCreation.create_url(job_id: job.id, status: 'completed')
    ObjectCreation.create_url(job_id: job.id, status: 'inprogress' )

    expect( job.get_status ).to eq(completed: 1, inprogress: 1)
  end
end
