require 'rails_helper'

RSpec.describe Url do

  it "validates the uri" do
    url = ObjectCreation.create_url(address: 'http://www.chase.com/')
    url.update_uri('https://www.chase.com/')

    expect(url.address).to eq('https://www.chase.com/')
  end
end